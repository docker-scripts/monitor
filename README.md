# Monitoring with Grafana, Prometheus, etc.

## Installation

  - First install `ds` and `revproxy`:
    + https://gitlab.com/docker-scripts/ds#installation
    + https://gitlab.com/docker-scripts/revproxy#installation

  - Then get the scripts: `ds pull monitor`

  - Create a directory for the container: `ds init monitor @mon.example.org`

  - Fix the settings: `cd /var/ds/mon.example.org/ ; vim settings.sh`

  - Make the container: `ds make`

## Other commands

```
ds stop
ds start
ds shell
ds help
```
