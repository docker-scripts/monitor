include(jammy)

RUN apt install --yes \
        curl wget apache2-utils \
	gnupg2 apt-transport-https software-properties-common \
        nginx ssl-cert

# install yq
RUN wget https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 -O /usr/local/bin/yq &&\
    chmod +x /usr/local/bin/yq

# install grafana
RUN curl -s https://packages.grafana.com/gpg.key | gpg --dearmor > /etc/apt/trusted.gpg.d/grafana.gpg &&\
    echo 'deb [signed-by=/etc/apt/trusted.gpg.d/grafana.gpg] https://packages.grafana.com/oss/deb stable main' \
        > /etc/apt/sources.list.d/grafana.list &&\
    apt update &&\
    apt install --yes grafana
RUN cp -a /etc/grafana /etc/grafana1
    
# download prometheus
RUN curl -s https://api.github.com/repos/prometheus/prometheus/releases/latest \
        | grep browser_download_url \
        | grep linux-amd64 \
        | cut -d '"' -f 4 \
        | wget -q -i- -O /root/prometheus.tgz

# download node_exporter
RUN curl -s https://api.github.com/repos/prometheus/node_exporter/releases/latest \
        | grep browser_download_url \
        | grep linux-amd64 \
        | cut -d '"' -f 4 \
	| wget -q -i- -O /root/node_exporter.tgz
