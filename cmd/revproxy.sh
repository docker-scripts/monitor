rename_function cmd_revproxy global_cmd_revproxy
cmd_revproxy() {
    global_cmd_revproxy "$@"
    [[ $1 == 'add' ]] &&  _custom_revproxy_config
}

_custom_revproxy_config() {
    local config_file=$(ds revproxy path)
    cp $APPDIR/misc/revproxy.conf $config_file

    sed -i $config_file \
        -e "s/example\.org/$DOMAIN/"

    # reload the new configuration
    ds @revproxy reload
}
