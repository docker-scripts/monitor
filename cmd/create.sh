rename_function cmd_create orig_cmd_create
cmd_create() {
    mkdir -p etc/{grafana,prometheus}
    orig_cmd_create \
        --mount type=bind,src=$(pwd)/etc/grafana,dst=/etc/grafana \
        --mount type=bind,src=$(pwd)/etc/prometheus,dst=/etc/prometheus \
        "$@"    # accept additional options, e.g.: --privileged
}
