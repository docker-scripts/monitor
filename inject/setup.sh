#!/bin/bash -x

source /host/settings.sh

main() {
    setup_node_exporter
    setup_prometheus
    setup_grafana
    setup_nginx

    start_services
}

setup_node_exporter() {
    tar xfz /root/node_exporter.tgz
    cd node_exporter*/
    
    cp node_exporter /usr/local/bin/

    cd ..
    rm -rf node_exporter*/
    rm /root/node_exporter.tgz

    # create a systemd service
    tee /etc/systemd/system/node_exporter.service <<EOF
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=default.target
EOF
    systemctl daemon-reload
}

setup_prometheus() {
    # create a group and user
    groupadd --system prometheus
    useradd -s /sbin/nologin --system -g prometheus prometheus

    # extract the downloaded archive
    tar xfz /root/prometheus.tgz
    rm /root/prometheus.tgz
    cd prometheus*

    # install the binaries
    mv prometheus promtool /usr/local/bin/

    # setup initial configuration
    [[ -f /etc/prometheus/prometheus.yml ]] || config_prometheus

    # cleanup
    cd ..
    rm -rf prometheus*

    # create the data directory
    mkdir -p /var/lib/prometheus
    chown -R prometheus:prometheus /var/lib/prometheus/

    # create a systemd service
    create_prometheus_systemd_service
}

create_prometheus_systemd_service() {
    tee /etc/systemd/system/prometheus.service <<'EOF'
[Unit]
Description=Prometheus
Documentation=https://prometheus.io/docs/introduction/overview/
Wants=network-online.target
After=network-online.target

[Service]
Type=simple
User=prometheus
Group=prometheus
ExecReload=/bin/kill -HUP $MAINPID
ExecStart=/usr/local/bin/prometheus \
  --config.file=/etc/prometheus/prometheus.yml \
  --storage.tsdb.path=/var/lib/prometheus \
  --web.console.templates=/etc/prometheus/consoles \
  --web.console.libraries=/etc/prometheus/console_libraries \
  --web.listen-address=0.0.0.0:9090 \
  --web.config.file=/etc/prometheus/web.yml

SyslogIdentifier=prometheus
Restart=always

[Install]
WantedBy=multi-user.target
EOF
    systemctl daemon-reload
}

config_prometheus() {
    mv prometheus.yml /etc/prometheus/
    mv consoles/ console_libraries/ /etc/prometheus/
    
    #for d in rules rules.d files_sd; do
    #    mkdir -p /etc/prometheus/$d
    #done
    
    # secure with basic authentication
    local hashed_passwd=$(htpasswd -nbB $ADMIN_USER $ADMIN_PASS | cut -d: -f2)
    tee /etc/prometheus/web.yml <<EOF
basic_auth_users:
    $ADMIN_USER: $hashed_passwd
EOF

    # set the basic_auth on the scraper config
    expression='with(
                  (.scrape_configs.[] | select(.job_name == "prometheus"));
                  . *= load("/dev/stdin")
                )'
    cat <<EOF | tee /dev/tty | yq "$expression" -i /etc/prometheus/prometheus.yml
basic_auth:
  username: '$ADMIN_USER'
  password: '$ADMIN_PASS'
EOF

    # add a scraper for the node_exporter
    expression='del(
                    .scrape_configs.[] | select(.job_name == "node_exporter")
                ) 
                | . *=+ load("/dev/stdin")
               '
    cat <<EOF | tee /dev/tty | yq "$expression" -i /etc/prometheus/prometheus.yml
scrape_configs:
  - job_name: "node_exporter"
    static_configs:
      - targets: ["localhost:9100"]
EOF
}

setup_grafana() {
    # check whether grafana.ini already exists
    if [[ -f /etc/grafana/grafana.ini ]]; then
        chgrp grafana -R /etc/grafana/
        return
    fi

    # we are installing for the first time
    # copy config files to the mounted directory
    cp -a /etc/grafana1/* /etc/grafana/

    # create a basic ini file
    cp /etc/grafana/{grafana.ini,grafana.sample.ini}
    tee /etc/grafana/grafana.ini <<EOF
[server]
http_addr = localhost
domain = $DOMAIN

#[users]
#allow_sign_up = false

#[auth.anonymous]
#enabled = false

[security]
admin_user = $ADMIN_USER
admin_password = $ADMIN_PASS
admin_email = $ADMIN_EMAIL
EOF
    chgrp grafana /etc/grafana/grafana.ini

    # create a datasource for prometheus
    tee /etc/grafana/provisioning/datasources/prometheus.yaml <<EOF
apiVersion: 1
datasources:
  - name: Prometheus
    type: prometheus
    access: proxy
    url: http://localhost:9090
    basicAuth: true
    basicAuthUser: $ADMIN_USER
    isDefault: true
    jsonData:
      httpMethod: POST
      tlsSkipVerify: true
    secureJsonData:
      basicAuthPassword: $ADMIN_PASS
    readOnly: false
EOF
}

setup_nginx() {
    tee /etc/nginx/sites-available/default <<'EOF'
# this is required to proxy Grafana Live WebSocket connections.
map $http_upgrade $connection_upgrade {
    default upgrade;
    '' close;
}

server {
    listen      80;
    server_name mon.example.org;
    rewrite     ^   https://$server_name$request_uri? permanent;
}

server {
    listen      443 ssl http2;
    server_name mon.example.org;

    root /usr/share/nginx/html;
    index index.html index.htm;

    include snippets/snakeoil.conf;

    access_log /var/log/nginx/grafana-access.log;
    error_log /var/log/nginx/grafana-error.log;

    location /prom/ {
        proxy_set_header Host $http_host;
        proxy_pass http://localhost:9090/;
    }

    location / {
        proxy_set_header Host $http_host;
        proxy_pass http://localhost:3000/;
    }

    # Proxy Grafana Live WebSocket connections.
    location /api/live {
        rewrite  ^/(.*)  /$1 break;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header Host $http_host;
        proxy_pass http://localhost:3000/;
    }
}
EOF

    sed -i /etc/nginx/sites-available/default \
        -e "s/mon.example.org/$DOMAIN/"

    systemctl restart nginx
}

start_services() {
    systemctl enable grafana-server
    systemctl start grafana-server

    systemctl enable prometheus
    systemctl start prometheus

    systemctl enable node_exporter
    systemctl start node_exporter

    systemctl restart nginx
}

main "$@"
